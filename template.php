<?php
/**
 * User: Adrian Tello <adriantl7@gmail.com>
 * Date: 26.08.15
 * Time: 20:24
 */

/**
 * Implements hook_preprocess_page().
 */
function sparkling_reloaded_preprocess_page(&$variables) {
  if ($variables['logo']) {
    //TODO calculate and cache
    $variables['logo_width'] = 65;
    $variables['logo_height'] = 73;
  }

  $variables['content_column_class_array'] = array(
    'main-content-inner',
    'col-sm-12'
  );

  // Add information about the number of sidebars.
  if (!empty($variables['page']['sidebar_first'])) {
    $variables['content_column_class_array'][] = 'col-md-8';
  }

  drupal_add_css('//fonts.googleapis.com/css?family=Open+Sans%3A400italic%2C400%2C600%2C700%7CRoboto+Slab%3A400%2C300%2C700&ver=4.2.4', 'external');
}

/**
 * Implements hook_process_page().
 */
function sparkling_reloaded_process_page(&$variables) {
  $variables['content_column_class'] = implode(' ', $variables['content_column_class_array']);
}

/**
 * Implements hook_preprocess_comment().
 */
function sparkling_reloaded_preprocess_comment(&$variables) {
  if (!isset($variables['title_attributes_array']['class'])) {
    $variables['title_attributes_array']['class'] = [];
  }
  $variables['title_attributes_array']['class'][] = 'fn';

  $link_class = array(
    'comment-delete' => 'btn-danger',
    'comment-edit' => 'btn-warning',
    'comment-reply' => 'btn-primary'
  );

  foreach ($variables['content']['links']['comment']['#links'] as $link_name => &$link) {
    $link['attributes']['class'][] = 'btn';
    $link['attributes']['class'][] = 'btn-sm';

    if (isset($link_class[$link_name])) {
      $link['attributes']['class'][] = $link_class[$link_name];
    }
    else {
      $link['attributes']['class'][] = 'btn-default';
    }
  }
}

/**
 * Implements hook_preprocess_node().
 */
function sparkling_reloaded_preprocess_node(&$variables) {
  $link_class = array(
    'node-readmore' => 'btn-danger'
  );

  $variables['content']['links']['node']['#attributes']['class'][] = 'node-links';
  foreach ($variables['content']['links']['node']['#links'] as $link_name => &$link) {
    $link['attributes']['class'][] = 'btn';
    $link['attributes']['class'][] = 'btn-sm';

    if (isset($link_class[$link_name])) {
      $link['attributes']['class'][] = $link_class[$link_name];
    }
    else {
      $link['attributes']['class'][] = 'btn-default';
    }
  }
}

////////////////////////////////////////////////////////////////////////////////
//Blocks
////////////////////////////////////////////////////////////////////////////////
/**
 * Implements hook_preprocess_block().
 */
function sparkling_reloaded_preprocess_block(&$variables) {
  if (!($variables['block']->region === 'content' && $variables['block']->module === 'system' && $variables['block']->delta === 'main')) {
    $variables['classes_array'][] = 'widget';
  }

  switch ($variables['block']->module) {
    case "comment":
      if ($variables['block']->delta === 'recent') {
        $variables['classes_array'][] = 'widget_recent_comments';
      }
    break;
    default:

      break;
  }
}