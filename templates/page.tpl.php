<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<header id="masthead" role="banner" class="<?php print $navbar_classes; ?>">
  <nav class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="row">
        <div class="site-navigation-inner col-sm-12">
          <div class="navbar-header">
            <button type="button" class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>

            <?php if($logo){ ?>
              <div id="logo">
                <a href="<?php print $front_page ?>"><img src="<?php print $logo; ?>"  height="<?php print $logo_height; ?>" width="<?php print $logo_width; ?>" alt="<?php echo print (!empty($site_name)?$site_name:t('Logo')); ?>"/></a>
              </div>
            <?php }else if (!empty($site_name)){ ?>
              <div id="logo">
                <span class="site-name">
                  <a class="navbar-brand" href="<?php print $front_page ?>" title="<?php echo print $site_name; ?>" rel="home">
                    <?php print $site_name; ?>
                  </a>
                </span>
              </div>
            <?php } ?>
          </div>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </nav>
</header>

<div id="content" class="site-content">
  <div class="top-section">
    <?php if(theme_get_setting('sparkling_call_to_action_enabled')): ?>
      <div class="cfa">
        <div class="container">
          <div class="col-sm-8">
            <span class="cfa-text"><?php print check_plain(theme_get_setting('sparkling_call_to_action_text')); ?></span>
          </div>
          <div class="col-sm-4">
            <?php print l(check_plain(theme_get_setting('sparkling_call_to_action_button_text')), theme_get_setting('sparkling_call_to_action_path'), array('attributes' => array('class' => array('btn', 'btn-lg', 'cfa-button')))); ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
  </div>
  <div class="container main-content-area">
    <div class="row">
      <div id="primary" class="content-area">
        <div class="<?php print $content_column_class; ?>">
          <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main"><?php print render($page['content']); ?></main>
          </div>
        </div>
        <?php if (!empty($page['sidebar_first'])): ?>
          <div id="secondary" class="widget-area col-sm-12 col-md-4" role="complementary">
            <?php print render($page['sidebar_first']); ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
<div id="footer-area">
  <?php if(!empty($page['footer'])): ?>
    <div class="container footer-inner">
      <div class="row">
        <?php print render($page['footer']); ?>
      </div>
    </div>
  <?php endif; ?>

  <?php if(!empty($page['footer_social']) || !empty($page['footer_links']) || !empty($page['footer_info'])): ?>
    <footer id="colophon" class="site-footer" role="contentinfo">
      <div class="site-info container">
        <div class="row">
          <?php print render($page['footer_social']); ?>
          <nav role="navigation" class="col-md-6">
            <?php print render($page['footer_links']); ?>
          </nav>
          <div class="copyright col-md-6">
            <?php print render($page['footer_info']); ?>
          </div>
        </div>
      </div>
      <div class="scroll-to-top"><i class="fa fa-angle-up"></i></div>
    </footer>
  <?php endif; ?>
</div>
