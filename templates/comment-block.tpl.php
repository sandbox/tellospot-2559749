<ul id="recentcomments">
<?php
  $number = variable_get('comment_block_count', 10);
  foreach (comment_get_recent($number) as $comment):
    $user = user_load($comment->uid);
    $node = node_load($comment->nid);
    ?>
    <li class="recentcomments">
      <?php
        print t(
          '!username on !post',
          array(
            '!username' => '<span class="comment-author-link">' . theme('username', array('account' => $user)) . '</span>',
            '!post' => l($node->title, 'node/' . $node->nid))
        );
      ?>
    </li>
  <?php
  endforeach;
  ?>
</ul>