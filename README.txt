*========= About Sparkling Reloaded ==========*/
Theme Name: Sparkling reloaded
Platform: Drupal

Author: Adrian Tello
Author URI: http://www.tellospot.com

GPL v3 licensed.

*========= About original Theme =========*/

Theme Name: Sparkling
Theme URI: http://colorlib.com/wp/sparkling/
Platform: Wordpress
Version: 1.9.3
Tested up to: WP 4.3

Author: Aigars Silkalns
Author URI: http://colorlib.com/wp/
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl.html
-------------------------------------------------------
Sparkling theme, Copyright 2014-2015 colorlib.com
Sparkling WordPress theme is distributed under the terms of the GNU GPL
Sparkling is based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc.