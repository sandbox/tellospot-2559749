<?php
/**
 * @file
 * theme-settings.php
 *
 * Provides theme settings and hides some default settings set for the theme
 *
 * @see theme/settings.inc
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function sparkling_reloaded_form_system_theme_settings_alter(&$form, $form_state, $form_id = NULL) {
  // Do not add Bootstrap specific settings to non-bootstrap based themes.
  $theme = !empty($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : FALSE;
  // Global settings.
  if ($theme === FALSE) {
    return;
  }

  $form['components']['navbar']['#access'] = FALSE; //Hide the navbar config -> Its already configured by the theme
  $form['components']['region_wells']['bootstrap_region_well-sidebar_first']['#access'] = FALSE; //Hide the navbar config -> Its already configured by the theme

  $form['sparkling'] = array(
    '#type' => 'vertical_tabs',
    '#prefix' => '<h2><small>' . t('Sparkling Settings') . '</small></h2>',
    '#weight' => -20,
  );

  //Call to action
  $form['sparkling']['cta'] = array(
    '#type' => 'fieldset',
    '#title' => t('Call to action'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['sparkling']['cta']['sparkling_call_to_action_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable call to action'),
    '#default_value' => theme_get_setting('sparkling_call_to_action_enabled')
  );

  $form['sparkling']['cta']['sparkling_call_to_action_button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Text to show in the button'),
    '#default_value' => theme_get_setting('sparkling_call_to_action_button_text'),
    '#states' => array(
      'visible' => array(
        ':input[name="sparkling_call_to_action_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['sparkling']['cta']['sparkling_call_to_action_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Description text'),
    '#default_value' => theme_get_setting('sparkling_call_to_action_text'),
    '#states' => array(
      'visible' => array(
        ':input[name="sparkling_call_to_action_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['sparkling']['cta']['sparkling_call_to_action_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Action path'),
    '#default_value' => theme_get_setting('sparkling_call_to_action_path'),
    '#states' => array(
      'visible' => array(
        ':input[name="sparkling_call_to_action_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );
}